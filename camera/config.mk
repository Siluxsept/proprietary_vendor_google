# Google Camera
ifeq ($(TARGET_PRODUCT),aicp_barbet)
PRODUCT_PACKAGES += \
    GoogleCameraMWP
else
PRODUCT_PACKAGES += \
    GoogleCameraPX
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/permissions/com.google.android.GoogleCameraEng.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.GoogleCameraEng.xml
